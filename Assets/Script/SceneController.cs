using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BoardGame
{
    public class SceneController : MonoBehaviourPunCallbacks
    {
        public void LoadSceneByName(string sceneName){
            SceneManager.LoadScene(sceneName);
        }


    }
}