using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

namespace BoardGame
{
    public class RoomMaster : MonoBehaviourPunCallbacks
    {
        public void OnCreateButtonClick()
        {
            PhotonNetwork.CreateRoom("1234", new Photon.Realtime.RoomOptions { MaxPlayers = 3 });
        }

        public override void OnCreatedRoom()
        {
            base.OnCreatedRoom();
    
            Debug.Log("Creat Room Succes!!");
        }
    }
}