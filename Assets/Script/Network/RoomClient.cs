using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using TMPro;

namespace BoardGame
{
    public class RoomClient : MonoBehaviourPunCallbacks
    {
        public TextMeshProUGUI OutputText;

        public void onJointRoomClick(){
            PhotonNetwork.JoinRoom("1234");
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();

            OutputText.SetText("Joint Room Success!!");
        }
    }
}