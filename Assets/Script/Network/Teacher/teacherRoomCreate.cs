using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using System;

public class teacherRoomCreate : MonoBehaviourPunCallbacks
{
    public TextInput roomNameInput;
    public TextInput maxPlayerInput;

    [Header("Room Limitation")]
    public int minPlayer;
    public int maxPlayer;

    [Header("Create Room Event")]
    public UnityEvent onCreaateRoom;
    public UnityEvent onCreateRoomFail;

    public void onCreaateRoomClick()
    {
        StartCoroutine(tryToCreate());
    }

    IEnumerator tryToCreate()
    {
        yield return new WaitForSeconds(0.75f);

        if (maxPlayerInput.value == "")
        {
            onCreateRoomFail.Invoke();
            roomNameInput.setError("please fill room name");
            maxPlayerInput.setError("please fill max player");
            yield break;
        }

        int playerAmount = Int32.Parse(maxPlayerInput.value);

        if (playerAmount < minPlayer)
        {
            maxPlayerInput.setError("max player should be 2");
            yield break;
        }
        if (playerAmount > maxPlayer)
        {
            maxPlayerInput.setError("max player should be 2");
            yield break;
        }

        PhotonNetwork.CreateRoom(roomNameInput.value, new Photon.Realtime.RoomOptions
        {
            MaxPlayers = Int32.Parse(maxPlayerInput.value)
        });
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        onCreaateRoom.Invoke();

        PhotonNetwork.LeaveRoom();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log("return code : " + returnCode);
        Debug.Log("error messege : " + message);
        onCreateRoomFail.Invoke();
    }
}
