using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Events;

public class LoginHandler : MonoBehaviour
{
    public TMP_InputField usernameInputField;
    public TMP_InputField passwordInputField;

    [Header("Event")]
    public UnityEvent onLoginVerifySucces;
    public UnityEvent onLoginVerifyFail;

    public void onLoginClick()
    {
        string username = usernameInputField.text;
        string password = passwordInputField.text;

        if (username == "teacher" && password == "1234")
        {
            onLoginVerifySucces.Invoke();
        }
        else
        {
            onLoginVerifyFail.Invoke();
        }
    }
}
