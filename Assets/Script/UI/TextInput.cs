using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextInput : MonoBehaviour
{
    public TMP_InputField input;
    public TextMeshProUGUI errorText;

    public string value
    {
        get
        {
            return input.text;
        }
    }

    public void clear()
    {
        input.text = "";
    }

    public void setError(string errorMsg)
    {
        errorText.text = errorMsg;
    }

    public void clearError()
    {
        errorText.text = "";
        errorText.gameObject.SetActive(false);
    }
}
